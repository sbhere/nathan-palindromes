#!/usr/bin/python2

### Iterates through an input file, checking each line for nathan-
#	palindrome-compliance, and writing the compliant strings to the
#	outfile. 
#	A nathan-palindrome is a string identical to itself when the
#	odd-from-the-ends characters are swapped. E.g.: Gag, Diced.
#	Capitalization is irrelevant.

import itertools, sys, os

if __name__ == '__main__' :
	if len(sys.argv) < 3: 
		sys.exit('Takes two parameters after the script: an infile and an outfile.')
	print sys.argv[1], sys.argv[2]
	if os.path.isfile(sys.argv[1]): infile = sys.argv[1]
	else: sys.exit('Need a valid infile.')
	if os.path.isfile(sys.argv[2]): outfile = sys.argv[2]
	else: sys.exit('Need a valid outfile (must be already created).')
	with open(infile) as f:
		for line in f:
			sut = line.strip().lower()
			if sut == ''.join( \
				''.join(x) for x in itertools.izip_longest(
						sut[::-2],sut[1::2], fillvalue='')):
				print sut
				with open(outfile, 'a') as ofile:
					ofile.write(sut + '\n')

